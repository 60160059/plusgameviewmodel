package com.tinnapong.plusgame.game

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.tinnapong.plusgame.R
import com.tinnapong.plusgame.databinding.FragmentMultiplyBinding
import java.util.*

class MultipleFragment : Fragment() {

    private lateinit var viewModel: MultipleViewModel

    private lateinit var binding: FragmentMultiplyBinding

    private lateinit var viewModelFactory: MultipleViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMultiplyBinding>(
            inflater,
            R.layout.fragment_multiply,
            container,
            false
        )

        Log.i("GameFragment", "Called ViewModelProvider.get")
        viewModelFactory = MultipleViewModelFactory (
            lastCorrect = MultipleFragmentArgs.fromBundle(requireArguments()).correctScore,
            lastIncorrect = MultipleFragmentArgs.fromBundle(requireArguments()).incorrectScore
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MultipleViewModel::class.java)

        binding.multipleViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.btnBack.setOnClickListener{
            NavHostFragment.findNavController(this).navigate(MultipleFragmentDirections.actionFragmentMultiplyToTitleFragment (
                viewModel.correct.value!!,
                viewModel.incorrect.value!!
            )
            )
        }
        return binding.root
    }
}