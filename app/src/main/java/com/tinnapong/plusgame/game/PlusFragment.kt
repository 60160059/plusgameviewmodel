package com.tinnapong.plusgame.game

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.tinnapong.plusgame.R
import com.tinnapong.plusgame.databinding.FragmentPlusBinding

class PlusFragment : Fragment() {

    private lateinit var viewModel: GameViewModel

    private lateinit var binding: FragmentPlusBinding

    private lateinit var viewModelFactory: PlusViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentPlusBinding>(
            inflater,
            R.layout.fragment_plus,
            container,
            false
        )

        Log.i("GameFragment", "Called ViewModelProvider.get")
        viewModelFactory = PlusViewModelFactory (
            lastCorrect = PlusFragmentArgs.fromBundle(requireArguments()).correctScore,
            lastIncorrect = PlusFragmentArgs.fromBundle(requireArguments()).incorrectScore
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)
        binding.gameViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

            binding.btnBack.setOnClickListener{
            NavHostFragment.findNavController(this).navigate(PlusFragmentDirections.actionFragmentPlusToTitleFragment (
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
        return binding.root
    }
}