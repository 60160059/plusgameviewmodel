package com.tinnapong.plusgame.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MultipleViewModelFactory(private val lastCorrect: Int, private val lastIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MultipleViewModel::class.java)) {
            return MultipleViewModel(lastCorrect, lastIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}