package com.tinnapong.plusgame.game

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.tinnapong.plusgame.R
import com.tinnapong.plusgame.databinding.FragmentMinusBinding
import java.util.*


class MinusFragment : Fragment() {

    private lateinit var viewModel: MinusViewModel

    private lateinit var binding: FragmentMinusBinding

    private lateinit var viewModelFactory: MinusViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMinusBinding>(
            inflater,
            R.layout.fragment_minus,
            container,
            false
        )

        Log.i("GameFragment", "Called ViewModelProvider.get")
        viewModelFactory = MinusViewModelFactory (
            lastCorrect = MinusFragmentArgs.fromBundle(requireArguments()).correctScore,
            lastIncorrect = MinusFragmentArgs.fromBundle(requireArguments()).incorrectScore
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MinusViewModel::class.java)

        binding.minusViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner


        binding.btnBack.setOnClickListener{
            NavHostFragment.findNavController(this).navigate(MinusFragmentDirections.actionFragmentMinusToTitleFragment (
                viewModel.correct.value!!,
                viewModel.incorrect.value!!
            )
            )
        }
        return binding.root
    }

}