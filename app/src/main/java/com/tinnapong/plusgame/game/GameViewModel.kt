package com.tinnapong.plusgame.game

import android.graphics.Color
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class GameViewModel(lastCorrect: Int, lastIncorrect: Int) : ViewModel() {
    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct
    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect
    private val _firstnum = MutableLiveData<Int>()
    val firstnum: LiveData<Int>
        get() = _firstnum
    private val _secondnum = MutableLiveData<Int>()
    val secondnum: LiveData<Int>
        get() = _secondnum
    private val _ansNumber = MutableLiveData<Int>()
    val ansNumber: LiveData<Int>
        get() = _ansNumber
    private val _randomChoice = MutableLiveData<Int>()
    val randomChoice: LiveData<Int>
        get() = _randomChoice
    private val _firstChoice = MutableLiveData<Int>()
    val firstChoice: LiveData<Int>
        get() = _firstChoice
    private val _SecondChoice = MutableLiveData<Int>()
    val SecondChoice: LiveData<Int>
        get() = _SecondChoice
    private val _ThirdChoice = MutableLiveData<Int>()
    val ThirdChoice: LiveData<Int>
        get() = _ThirdChoice
    private val _alertText = MutableLiveData<String>()
    val alertText: LiveData<String>
        get() = _alertText
    private val _thisShow = MutableLiveData<Boolean>()
    val thisShow: LiveData<Boolean>
        get() = _thisShow
    private val _thisCorrect = MutableLiveData<Boolean>()
    val thisCorrect: LiveData<Boolean>
        get() = _thisCorrect

    init {
        _correct.value = lastCorrect
        _incorrect.value = lastIncorrect
        _firstnum.value = 0
        _secondnum.value = 0
        _ansNumber.value = 0
        _randomChoice.value = 0
        _firstChoice.value = 0
        _SecondChoice.value = 0
        _ThirdChoice.value = 0
        _alertText.value = ""
        _thisCorrect.value = false
        _thisShow.value = false
        Log.i("GameViewModel", "GameViewModel created!")
        startGame()
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }

    fun startGame() {
        _thisCorrect.value = false
        _thisShow.value = false
        randomNumber()
        pseudoNumber()
        setRandomButton()
    }

    private fun randomNumber() {
        _firstnum.value = (0 until 10).random()
        _secondnum.value = (0 until 10).random()
        _ansNumber.value = _firstnum.value!! + _secondnum.value!!
    }

    private fun pseudoNumber() {
        _randomChoice.value = Random().nextInt(2) + 1
    }

    fun scoreUp() {
        _thisCorrect.value = true
        _thisShow.value = true
        _correct.value = _correct.value?.plus(1)
        _alertText.value = "Correct!"
    }

    fun scoreDown() {
        _thisCorrect.value = false
        _thisShow.value = true
        _incorrect.value = _incorrect.value?.plus(1)
        _alertText.value = "Incorrect!"
    }

    fun processGame(button: Button) {
        if (button.text == ansNumber.value.toString()) {
            button.isEnabled = false
            scoreUp()
            Handler().postDelayed({
                button.isEnabled = true
                startGame()
            }, 1000)
        } else {
            scoreDown()
        }
    }

    private fun setRandomButton() {
            if (randomChoice.value == 1) {
                _firstChoice.value = ansNumber.value
                _SecondChoice.value = ansNumber.value?.plus(1)
                _ThirdChoice.value = ansNumber.value?.plus(2)
            } else if (randomChoice.value == 2) {
                _firstChoice.value = ansNumber.value?.minus(1)
                _SecondChoice.value = ansNumber.value
                _ThirdChoice.value = ansNumber.value?.plus(1)
            } else if (randomChoice.value == 3) {
                _firstChoice.value = ansNumber.value?.minus(2)
                _SecondChoice.value = ansNumber.value?.minus(1)
                _ThirdChoice.value = ansNumber.value
            }
    }
}