package com.tinnapong.plusgame.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PlusViewModelFactory(private val lastCorrect: Int, private val lastIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GameViewModel::class.java)) {
            return GameViewModel(lastCorrect, lastIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
