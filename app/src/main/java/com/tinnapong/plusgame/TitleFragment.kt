package com.tinnapong.plusgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.tinnapong.plusgame.databinding.FragmentTitleBinding
import com.tinnapong.plusgame.game.GameViewModel
import kotlinx.android.synthetic.main.fragment_title.*

class TitleFragment : Fragment() {

    private lateinit var binding: FragmentTitleBinding
    private lateinit var viewModel: TitleViewModel
    private lateinit var viewModelFactory: TitleViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater,
            R.layout.fragment_title,container,false)

        viewModelFactory = TitleViewModelFactory(
            lastCorrect = TitleFragmentArgs.fromBundle(requireArguments()).correctScore,
            lastIncorrect = TitleFragmentArgs.fromBundle(requireArguments()).incorrectScore
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(TitleViewModel::class.java)

        viewModel.correct.observe(viewLifecycleOwner, Observer { newCorrect ->
            binding.txtCorrect.text = "Correct: " + newCorrect.toString()
        })
        viewModel.incorrect.observe(viewLifecycleOwner, Observer { newIncorrect ->
            binding.txtIncorrect.text = "Incorrect: " + newIncorrect.toString()
        })

        binding.btnPlus.setOnClickListener { view : View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToFragmentPlus(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
        binding.btnMinus.setOnClickListener { view : View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToFragmentMinus(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
        binding.btnMultiply.setOnClickListener { view : View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToFragmentMultiply(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
        return binding.root
    }
}