package com.tinnapong.plusgame

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TitleViewModelFactory(private val lastCorrect: Int, private val lastIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TitleViewModel::class.java)) {
            return TitleViewModel(lastCorrect, lastIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
