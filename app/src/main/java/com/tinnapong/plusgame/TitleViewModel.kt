package com.tinnapong.plusgame

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TitleViewModel(lastCorrect: Int, lastIncorrect: Int) : ViewModel() {
    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct
    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    init {
        _correct.value = lastCorrect
        _incorrect.value = lastIncorrect
    }

}
